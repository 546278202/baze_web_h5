let MIXIN = {
    data() {
        return {
            name: 'mixin',
            defalutLogoUrl: 'this.src="' + require("@/assets/nodata.png") + '"', 
        }
    },
    created() {
    },
    mounted() {

    },
    methods: {
        // 退出登录
        handleSignOut(){
            this.$dialog.confirm({  title: '', message: '退出登录？', }) .then(() => {
                this.axios .get(this.Global.BASE_URL + "/index.php?r=web/site/logout").then(response => {
                    if (response.code == "0") {
                        this.$store.commit('changeLogin', null);
                        localStorage.removeItem("store")
                        window.location.reload()
                    }
                }).catch(response => {
                    this.$store.commit('changeLogin', null);
                    localStorage.removeItem("store")
                    window.location.reload()
                });
            })
            .catch(() => {
                console.log("操作取消了")
            });
        }
    }
};
export default MIXIN