import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import router from './router'
import axios from 'axios'
import qs from 'qs'
import Global from './utils/base'
import Vant from 'vant'
import 'vant/lib/index.css'
import './utils/rem.js';
// 引入amfe-flexible
import 'amfe-flexible/index'
/*引入公共样式*/
import './style/common.css'
import './style/vant.css'

import { Toast } from 'vant';

Vue.use(Vant)
axios.defaults.withCredentials = true
Vue.prototype.axios = axios;
Vue.prototype.Global = Global;
Vue.config.productionTip = false



let needLoadingRequestCount = 0 // 声明一个对象用于存储请求个数
function startLoading() {
    Toast.loading({
        duration: 0, // 持续展示 toast
        forbidClick: true,
        // overlay:true,
        message: "加载中..."
    });
}

function endLoading() {
    Toast.clear();

}

function showFullScreenLoading() {
    if (needLoadingRequestCount === 0) {
        startLoading()
    }
    needLoadingRequestCount++
}

function hideFullScreenLoading() {
    if (needLoadingRequestCount <= 0) return
    needLoadingRequestCount--
    if (needLoadingRequestCount === 0) {
        endLoading()
    }
}

// http request 拦截器
axios.interceptors.request.use(function(config) {
    if (config.isLoading != false) { // 如果配置了isLoading: false，则不显示loading
        showFullScreenLoading()
    }
    if (config.method == 'post') {
        config.headers['Content-Type'] = 'application/json; charset=utf-8';
    }
    if (config.method == 'get') {
        config.data = qs.stringify(config.data);
    }
    if (config.method == 'patch') {
        config.headers['Content-Type'] = 'application/json; charset=utf-8';
    }
    return config
}, error => {
    hideFullScreenLoading()
    return Promise.reject(error.response)
});

// http response 拦截器
axios.interceptors.response.use(response => {
    // hideFullScreenLoading()
    return response.data;
    
}, error => {
    if (error.response) {
        switch (error.response.status) {
            case 401:
                Toast('未授权，请登录');
                store.commit('changeLogin', null);
                localStorage.removeItem("store")
                router.replace('/login');
                // next({ path: '/login', query: { Rurl: to.fullPath } })
                break;
            case 404:
                Toast('response.msg 404');
                break;
            case 500:
                Toast(`服务器异常500,请联系管理员！`);
        }
    } else {
        Toast('请求错误或服务器异常,请联系管理员！');
    }
    // endLoading()
    
    return Promise.reject(error);
});

//路由跳转
router.beforeEach((to, from, next) => {
    /* 路由发生变化修改页面title */
    if (to.meta.title) {
        document.title = to.meta.title
    }
    if(to.meta.auth){
        if (JSON.parse(localStorage.getItem("store"))==null || JSON.parse(localStorage.getItem("store")).baseUser==null) {
            store.commit('changeLogin', null);
            // router.replace('/login');
            localStorage.removeItem("store")
            next({path: '/login', query: { backurl:to.fullPath}})
        }
        next()
    }else{
        next()
    }
})
new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
