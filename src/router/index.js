import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
//解决路由跳转原路由或者刷新出错
const originalReplace = Router.prototype.replace;
Router.prototype.replace = function replace(location) {
    return originalReplace.call(this, location).catch(err => err);
};
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
export default new Router({
    routes: [{
            path: '/',
            name: 'home',
            component: () =>import ('@/pages/home'),
            meta: { title: '首页',  auth: false }
        },
        {
            path: '/home',
            name: 'home',
            component: () =>import ('@/pages/home'),
            meta: { title: '首页',  auth: false }
        },
        {
            path: '/live',
            name: 'live',
            component: () =>import ('@/pages/live'),
            meta: { title: '课程直播',  auth: false }
        },
        {
            path: '/liveList',
            name: 'liveList',
            component: () =>import ('@/pages/liveList'),
            meta: { title: '直播课',  auth: false }
        },
        {
            path: '/videoList',
            name: 'videoList',
            component: () =>import ('@/pages/videoList'),
            meta: { title: '录播课',  auth: false }
        },
        {
            path: '/video',
            name: 'video',
            component: () =>import ('@/pages/video'),
            meta: { title: '课程视频',  auth: false }
        },
        {
            path: '/educationPromotion',
            name: 'educationPromotion',
            component: () =>import ('@/pages/educationPromotion'),
            meta: { title: '学历',  auth: false }
        },
        {
            path: '/educationPromotion/collegesList',
            name: 'educationPromotion/collegesList',
            component: () =>import ('@/pages/educationPromotion/collegesList/index'),
            meta: { title: '院校列表',  auth: false }
        },
        {
            path: '/educationPromotion/collegesList/detail',
            name: 'educationPromotion/collegesList/detail',
            component: () =>import ('@/pages/educationPromotion/collegesList/detail'),
            meta: { title: '院校详情',  auth: false }
        },
        {
            path: '/educationPromotion/majorList',
            name: 'educationPromotion/majorList',
            component: () =>import ('@/pages/educationPromotion/majorList/index'),
            meta: { title: '专业列表',  auth: false }
        },
        {
            path: '/educationPromotion/majorList/detail',
            name: 'educationPromotion/majorList/detail',
            component: () =>import ('@/pages/educationPromotion/majorList/detail'),
            meta: { title: '专业详情',  auth: false }
        },
        {
            path: '/educationPromotion/majorDetailSubmit',
            name: 'educationPromotion/majorDetailSubmit',
            component: () =>import ('@/pages/educationPromotion/majorDetailSubmit'),
            meta: { title: '提交报名',  auth: false }
        },
        {
            path: '/userCenter',
            name: 'userCenter',
            component: () =>import ('@/pages/userCenter'),
            meta: { title: '我的',  auth: false }
        },
        {
            path: '/userCenter/lesson',
            name: 'userCenter/lesson',
            component: () =>import ('@/pages/userCenter/lesson'),
            meta: { title: '我的课程',  auth: true }
        },
        {
            path: '/userCenter/collection',
            name: 'userCenter/collection',
            component: () =>import ('@/pages/userCenter/collection'),
            meta: { title: '我的收藏',  auth: true }
        },
        {
            path: '/userCenter/teacher',
            name: 'userCenter/teacher',
            component: () =>import ('@/pages/userCenter/teacher'),
            meta: { title: '我的导师',  auth: true }
        },
        {
            path: '/videoDetail',
            name: 'videoDetail',
            component: () =>import ('@/pages/videoDetail'),
            meta: { title: '录播详情',  auth: false }
        },
        {
            path: '/liveDetail',
            name: 'liveDetail',
            component: () =>import ('@/pages/liveDetail'),
            meta: { title: '直播详情',  auth: false }
        },
        {
            path: '/login',
            name: 'login',
            component: () =>import ('@/pages/login'),
            meta: { title: '登录',  auth: false }
        },
        {
            path: '/paymentPage',
            name: 'paymentPage',
            component: () =>import ('@/pages/paymentPage'),
            meta: { title: '支付结算',  auth: true }
        },
        {
            path: '/order',
            name: 'order',
            component: () =>import ('@/pages/order/index'),
            meta: { title: '订单',  auth: true },
          
        },
        {
            path: '/order/detail',
            name: 'order/detail',
            component: () =>import ('@/pages/order/detail'),
            meta: { title: '订单详情',  auth: true }
        },
        {
            path: '/paySuccessPage',
            name: 'paySuccessPage',
            component: () =>import ('@/pages/paySuccessPage'),
            meta: { title: '支付成功',  auth: true },
        },
        {
            path: '/address',
            name: 'address',
            component: () =>import ('@/pages/address/index'),
            meta: { title: '新增/修改',  auth: true },
           
        },
        {
            path: '/address/list',
            name: 'address/list',
            component: () =>import ('@/pages/address/list'),
            meta: { title: '地址列表',  auth: true },
           
        },
        {
            path: '/userCenter/information',
            name: 'userCenter/information',
            component: () =>import ('@/pages/userCenter/information'),
            meta: { title: '个人资料',  auth: true },
           
        },
        {
            path: '/certificate',
            name: 'certificate',
            component: () =>import ('@/pages/certificate'),
            meta: { title: '我的证书',  auth: true },
        },
        {
            path: '/query_education',
            name: 'query_education',
            component: () =>import ('@/pages/query_education'),
            meta: { title: '查询证书',  auth: false },
        },
        {
            path: '/cooperation_alliance',
            name: 'cooperation_alliance',
            component: () =>import ('@/pages/cooperation_alliance'),
            meta: { title: '合作加盟',  auth: false },
        },
        

        

        
        
        {path: '*', redirect: '/' }
    ],
    mode: "history",
    scrollBehavior: () => ({ x: 0, y: 0 }),
})

